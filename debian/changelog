virtaal (0.7.1+git20191021+ds1-2) unstable; urgency=medium

  * Add missing dependencies on python3-six.
  * Move to debhelper-compat 13.
  * Update Standards-Version to 4.5.0 (no changes required).

 -- Stuart Prescott <stuart@debian.org>  Sat, 15 Aug 2020 12:25:48 +1000

virtaal (0.7.1+git20191021+ds1-1) unstable; urgency=medium

  * New git snapshot for Python 3 and Gtk3 support.
    - update build and runtime dependencies to Python 3 versions
      (Closes: #938781).
    - update build and runtime dependencies to Gtk 3 versions
      (Closes: #885493).
    - drop patches already applied upstream.
    - refresh patches.
    - add extra patches for Python 3 porting.
  * Mark trivial application autopkgtest as 'superficial'.
  * Build the documentation and include it in the package.
  * Update Standards-Version to 4.4.1 (no changes required).

 -- Stuart Prescott <stuart@debian.org>  Tue, 29 Oct 2019 00:15:28 +1100

virtaal (0.7.1-7) unstable; urgency=medium

  * Depend on python-diff-match-patch module and cherry-pick upstream commits
    to us it rather than the embedded copy from python-translate.
  * Update Uploaders list, removing  Christian Perrier and Omar Campagne.
    Thank you both for your contribution! (Closes: #927577)
  * Update Standards-Version to 4.4.0 (no changes required).
  * Update to debhelper compat 12.

 -- Stuart Prescott <stuart@debian.org>  Mon, 08 Jul 2019 15:57:50 +1000

virtaal (0.7.1-6) unstable; urgency=medium

  * Cherry pick upstream patch to follow redirects, with thanks to Török Edwin
    (Closes: #788421).
  * Update Standards-Version to 4.1.4 (no changes required).

 -- Stuart Prescott <stuart@debian.org>  Fri, 06 Apr 2018 20:29:00 +1000

virtaal (0.7.1-5) unstable; urgency=medium

  * Point Vcs-* URLs to salsa.debian.org.
  * Update Standards-Version to 4.1.3 (no changes required).
  * Bump debhelper compat to 11.
  * Switch dependency to python-gobject-2 (Closes: #890176).
  * Update dependency to python-translate for new translate-toolkit packaging
    (Closes: #826856).
  * Fix kdialog use for Plasma 5.

 -- Stuart Prescott <stuart@debian.org>  Thu, 22 Feb 2018 00:55:18 +1100

virtaal (0.7.1-4) unstable; urgency=medium

  * permit stderr from autopkgtests

 -- Stuart Prescott <stuart@debian.org>  Tue, 07 Nov 2017 12:22:35 +1100

virtaal (0.7.1-3) unstable; urgency=medium

  * Remove Recommends on old python-gtkspell package; there is no spell
    checking in virtaal until it is ported to gtkspellcheck (Closes: #809657).
  * Don't assume that kdialog is installed in KDE sessions.

 -- Stuart Prescott <stuart@debian.org>  Tue, 10 Jan 2017 23:42:24 +1100

virtaal (0.7.1-2) unstable; urgency=medium

  * Remove Nicolas François (Nekral) from uploaders, with thanks for work
    over many years (Closes: #832384).
  * Add Stuart Prescott to Uploaders.
  * Bump Standards-Version to 3.9.8 (no changes required).
  * Update Homepage to new location.
  * Update Vcs fields to new git repository.
  * Drop obsolete Debian menu file.
  * Switch to debhelper compat 9 and pybuild for build system.
  * Extend package description.

 -- Stuart Prescott <stuart@debian.org>  Wed, 27 Jul 2016 01:30:51 +1000

virtaal (0.7.1-1) unstable; urgency=low

  * New upstream release.
  * debian/patches: refresh patches

 -- Omar Campagne <ocampagne@gmail.com>  Wed, 11 Jan 2012 16:47:36 +0100

virtaal (0.7.0-2) unstable; urgency=low

  [ Omar Campagne ]
  * debian/control: Drop quilt from build-depends. Not necessary.
  * debian/control: Drop python-simplejson. Virtaal automatically checks
    and defaults to built-in python 2.6 json if python-simplejson is not
    installed. Closes: #612218

  [ Christian Perrier ]
  * Depend on libreoffice-common instead of openoffice.org-common
    Closes: #651667

 -- Christian Perrier <bubulle@debian.org>  Sun, 11 Dec 2011 14:02:55 +0100

virtaal (0.7.0-1) unstable; urgency=low

  [ Omar Campagne ]
  * New upstream version.
  * Added debian/patches/declare_packaged_version to avoid dependency
    checking on startup as well as the "--profile" function on packaged
    versions.
  * debian/virtaal.1: Delete "--profile" info from the manpage,
    now disabled.
    Change spelling dependency package recommendation from
    openoffice.org-common to libreoffice-common.
  * debian/rules: Create minimal dh-7 file.
  * debian/source/options: Added option to ignore MO files, hence
    allowing to build twice in a row.
  * debian/control: Bump translate-toolkit dependency to 1.9.0
  * debian/control: Bump Standards version to 3.9.2. No changes
  * Drop python-central for dh_python2. Closes: #617142
  * debian/control: Set maintainer to Debian l10n devel group,
    and add Christian Perrier, Nicolas François and myself as uploaders
  * debian/rules: Drop dh_clean -k for dh_prep
  * debian/virtaal.1: Recommend python-gtkspell in the manpage instead
    of python-gnome2-extras (now unavaliable)
    Delete --terminology option, doesn't exist
    Add and correct shortcuts according to new upstream
    version, add link to complete list of shortcuts
  * debian/patches/debian_openoffice_autocorr_files: Rename and update
    to libreoffice's path for autocorrection files
  * debian/patches: Refresh patches

  [ Christian Perrier ]
  * Switch to 3.0 source format

 -- Omar Campagne <ocampagne@gmail.com>  Fri, 03 Jun 2011 13:51:06 +0200

virtaal (0.6.1-0.1) unstable; urgency=low

  * Non-maintainer upload with maitnainer's agreement.
  * New upstream version. Closes: #585005

 -- Christian Perrier <bubulle@debian.org>  Fri, 30 Jul 2010 03:41:19 +0200

virtaal (0.5.2-1) unstable; urgency=low

  * New upstream release
  * debian/control: Bump version dependency of translate-toolkit to 1.5.1.
  * debian/control: Standards-Version bumped to 3.8.4. No changes.
  * debian/control: Add dependency on ${misc:Depends}
  * debian/control: Remove build dependency on python-all-dev.
  * debian/copyright: Updated

 -- Nicolas FRANCOIS (Nekral) <nicolas.francois@centraliens.net>  Tue, 02 Feb 2010 00:22:04 +0100

virtaal (0.5.1-1) unstable; urgency=low

  * New upstream release
  * debian/control: Added python-psycopg2 to the Recommended package. This is
    needed for the TinyTM plugin.

 -- Nicolas FRANCOIS (Nekral) <nicolas.francois@centraliens.net>  Mon, 11 Jan 2010 19:40:47 +0100

virtaal (0.5.0-1) unstable; urgency=low

  * New upstream release

 -- Nicolas FRANCOIS (Nekral) <nicolas.francois@centraliens.net>  Sun, 06 Dec 2009 12:51:22 +0100

virtaal (0.5.0~rc1-1) UNRELEASED; urgency=low

  * New upstream release
  * debian/control: This version of virtaal will depend on
    translate-toolkit >= 1.5.0
  * debian/patches/debian_openoffice_autocorr_files,
    debian/patches/fail_for_missing_dependencies: Refreshed patches.

 -- Nicolas FRANCOIS (Nekral) <nicolas.francois@centraliens.net>  Sat, 21 Nov 2009 14:53:56 +0100

virtaal (0.4.1-1) unstable; urgency=low

  * New upstream release
  * debian/control: Standards-Version bumped to 3.8.3. No changes.

 -- Nicolas FRANCOIS (Nekral) <nicolas.francois@centraliens.net>  Sun, 08 Nov 2009 23:17:08 +0100

virtaal (0.4.0-2) unstable; urgency=low

  * debian/control: Recommend python-gtkspell instead of python-gnome2-extras.
    Closes: #541599

 -- Nicolas FRANCOIS (Nekral) <nicolas.francois@centraliens.net>  Sat, 15 Aug 2009 02:38:22 +0200

virtaal (0.4.0-1) unstable; urgency=low

  * New upstream release

 -- Nicolas FRANCOIS (Nekral) <nicolas.francois@centraliens.net>  Mon, 10 Aug 2009 21:16:09 +0200

virtaal (0.4.0~rc2-1) experimental; urgency=low

  * New upstream release
  * debian/control: move from section python to section devel.

 -- Nicolas FRANCOIS (Nekral) <nicolas.francois@centraliens.net>  Mon, 03 Aug 2009 00:10:47 +0200

virtaal (0.4.0~rc1-1) experimental; urgency=low

  * New upstream release
    + debian/patches/glade_warnings: Removed (not needed anymore).
  * debian/control: Standards-Version bumped to 3.8.2. No changes.
  * debian/rules: Removed dh_desktop, this is now handled with a trigger.
  * debian/rules: Do not check the dependencies at build time. This avoid
    depending on too many python libraries at build time (--nodepcheck added)
  * debian/rules: Remove the generated (binary) MO files in the clean rule to
    allow building twice in a row
  * debian/patches/debian_remove_tests: Do not distribute the test directory.
  * debian/patches/fail_for_missing_dependencies: Fail when the dependencies
    are checked (and some dependencies are missing) during build time, instead
    of just issuing a warning.
  * debian/control: Added dependency: python-simplejson.

 -- Nicolas FRANCOIS (Nekral) <nicolas.francois@centraliens.net>  Sat, 25 Jul 2009 13:59:33 +0200

virtaal (0.3.1-3) unstable; urgency=low

  * debian/watch: Added watch file.

 -- Nicolas FRANCOIS (Nekral) <nicolas.francois@centraliens.net>  Sun, 01 Mar 2009 11:23:38 +0100

virtaal (0.3.1-2) unstable; urgency=low

  * debian/control: Added dependency on python-lxml. It is required for
    Virtaal, but only recommended by translate-toolkit. Closes: #517522

 -- Nicolas FRANCOIS (Nekral) <nicolas.francois@centraliens.net>  Sat, 28 Feb 2009 13:31:05 +0100

virtaal (0.3.1-1) unstable; urgency=low

  * New upstream release
  * debian/control, debian/virtaal.1: Fix Virtaal's name.

 -- Nicolas FRANCOIS (Nekral) <nicolas.francois@centraliens.net>  Wed, 18 Feb 2009 19:04:39 +0100

virtaal (0.3.0-1) experimental; urgency=low

  * New upstream release
  * debian/control: Added missing build dependency on translate-toolkit.
    Closes: #507267
  * debian/control: Recommend python-levenshtein to avoid warnings.
    Closes: #507207
  * debian/control: Added missing dependency on python-glade2.
    Closes: #507206
  * debian/control: Added dependency on python-pycurl.
  * debian/rules: build-stamp depends on $(QUILT_STAMPFN) instead of patch.
    This avoid building twice.
  * trunk/debian/patches/debian_openoffice_autocorr_files: Updated.
  * debian/patches/glade_warnings: Added patch to avoid glade warnings.

 -- Nicolas FRANCOIS (Nekral) <nicolas.francois@centraliens.net>  Tue, 10 Feb 2009 22:16:59 +0100

virtaal (0.2-1) unstable; urgency=low

  * New upstream release. Closes: #500954
  * debian/virtaal.1: Added manpage.
  * debian/control: Updated translate-toolkit dependency.

 -- Nicolas FRANCOIS (Nekral) <nicolas.francois@centraliens.net>  Sun, 19 Oct 2008 15:28:08 +0200

virtaal (0.2~rc1-2) experimental; urgency=low

  * Initial Debian release. Closes: #500954
  * debian/control: Add a versioned dependency on the translate-toolkit
    (>= 1.2.0~rc1)

 -- Nicolas FRANCOIS (Nekral) <nicolas.francois@centraliens.net>  Fri, 03 Oct 2008 21:51:07 +0200

virtaal (0.2~rc1-1) experimental; urgency=low

  * New upstream release.
  * debian/control: Standards-Version bumped to 3.8.0.
  * debian/control: Updated Homepage (point to the wiki page for Virtaal).

 -- Nicolas FRANCOIS (Nekral) <nicolas.francois@centraliens.net>  Thu, 02 Oct 2008 17:44:53 +0200

virtaal (0.1-1) UNRELEASED; urgency=low

  * Initial Release.

 -- Nicolas FRANCOIS (Nekral) <nicolas.francois@centraliens.net>  Sat, 10 May 2008 14:19:35 +0200
